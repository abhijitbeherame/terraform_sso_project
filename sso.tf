provider "aws" {
  region = "ap-south-1"
}

module "sso" {
  source  = "avlcloudtechnologies/sso/aws"

  permission_sets = {
    ViewOnlyAccess = {
      description      = "Provides read only access to AWS services and resources.",
      session_duration = "PT1H",
      managed_policies = ["arn:aws:iam::aws:policy/job-function/ViewOnlyAccess"]
    },
  }
  account_assignments = [
    {
      principal_name = "ReadOnlyGroup"
      principal_type = "GROUP"
      permission_set = "ViewOnlyAccess"
      account_ids    = ["XXXXXXXXXXXX"]
    },
  ]
}
