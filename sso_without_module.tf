provider "aws" {
     region = "ap-south-1"
}

data "aws_ssoadmin_instances" "sso-instances" {}
data "aws_ssoadmin_instances" "group" {}


####Permission Set Creation#####

resource "aws_ssoadmin_permission_set" "permission" {
  name         = "ReadOnlyAccess"
  instance_arn = tolist(data.aws_ssoadmin_instances.sso-instances.arns)[0]
  
}

data "aws_iam_policy_document" "document" {
statement {
    sid = "1"

    actions = [
                "ec2:Describe*",
    ]
    resources = [
           "*",
    ]
  }
}
resource "aws_ssoadmin_permission_set_inline_policy" "policy" {
  inline_policy      = data.aws_iam_policy_document.document.json
  instance_arn       = aws_ssoadmin_permission_set.permission.instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.permission.arn
}


####Account Assignment####

data "aws_identitystore_group" "group" {
  identity_store_id = tolist(data.aws_ssoadmin_instances.group.identity_store_ids)[0]

  filter {
    attribute_path  = "DisplayName"
    attribute_value = "ReadOnlyGroup"
  }
}

resource "aws_ssoadmin_account_assignment" "assignment" {
  instance_arn       = aws_ssoadmin_permission_set.permission.instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.permission.arn

  principal_id   = data.aws_identitystore_group.group.group_id
  principal_type = "GROUP"

  target_id   = "XXXXXXXXXXXX"
  target_type = "AWS_ACCOUNT"
}
